#include <windows.h>
#include <stdio.h>

#define MAXIMUM_FILESIZE 1024*1024*10 //10mb

static void CopyToClipboard(const char* text)
{
    HGLOBAL hText;
    char *pText;
    int len=strlen(text);
    if(!len)
        return;

    hText=GlobalAlloc(GMEM_DDESHARE|GMEM_MOVEABLE, len+1);
    pText=(char*)GlobalLock(hText);
    strcpy(pText, text);

    OpenClipboard(0);
    EmptyClipboard();
    if(!SetClipboardData(CF_OEMTEXT, hText))
        MessageBeep(MB_ICONERROR);
    else
        MessageBeep(MB_ICONINFORMATION);
    CloseClipboard();
}

static void CopyFileToClipboard(const char* filename)
{
    HANDLE hFile=CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if(hFile==INVALID_HANDLE_VALUE)
        return;
    unsigned int filesize=GetFileSize(hFile, 0);
    if(filesize>MAXIMUM_FILESIZE)
    {
        CloseHandle(hFile);
        return;
    }
    char* buffer=(char*)malloc(filesize+1);
    if(!buffer)
    {
        CloseHandle(hFile);
        return;
    }
    memset(buffer, 0, filesize+1);
    DWORD read=0;
    if(!ReadFile(hFile, buffer, filesize, &read, 0))
    {
        free(buffer);
        CloseHandle(hFile);
        return;
    }
    CloseHandle(hFile);
    int len=strlen(buffer);
    if(len)
        CopyToClipboard(buffer);
    free(buffer);
}

static const char szRegFileFormat[]="REGEDIT4\r\n\r\n[HKEY_CLASSES_ROOT\\*\\shell\\CopyFile]\r\n\r\n[HKEY_CLASSES_ROOT\\*\\shell\\CopyFile\\Command]\r\n@=\"\\\"%s\\\" \\\"%%1\\\"\"\r\n\r\n";

int main(int argc, char* argv[])
{
    if(argc<2)
    {
        if(MessageBoxA(0, "Do you want to register CopyFile?", "Question", MB_ICONQUESTION|MB_SYSTEMMODAL|MB_YESNO)==IDYES)
        {
            char szCurrentPath[MAX_PATH]="";
            if(GetModuleFileNameA(GetModuleHandleA(0), szCurrentPath, MAX_PATH))
            {
                char szRegFilePath[MAX_PATH]="";
                sprintf(szRegFilePath, "%s.reg", szCurrentPath);
                HANDLE hFile=CreateFileA(szRegFilePath, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
                if(hFile!=INVALID_HANDLE_VALUE)
                {
                    SetFilePointer(hFile, 0, 0, FILE_BEGIN);
                    SetEndOfFile(hFile);
                    char szRegFileText[1024]="";
                    char szEscapedCurrentPath[1024]="";
                    int len=strlen(szCurrentPath);
                    for(int i=0,j=0; i<len; i++)
                    {
                        if(szCurrentPath[i]=='\\')
                            j+=sprintf(szEscapedCurrentPath+j, "\\\\");
                        else
                            j+=sprintf(szEscapedCurrentPath+j, "%c", szCurrentPath[i]);
                    }
                    sprintf(szRegFileText, szRegFileFormat, szEscapedCurrentPath);
                    DWORD NumberOfBytesWritten=0;
                    if(WriteFile(hFile, szRegFileText, strlen(szRegFileText), &NumberOfBytesWritten, 0))
                    {
                        CloseHandle(hFile);
                        char szRegCommand[1024]="";
                        sprintf(szRegCommand, "regedit \"%s\"", szRegFilePath);
                        system(szRegCommand);
                    }
                    else
                        CloseHandle(hFile);
                    DeleteFileA(szRegFilePath);
                }
            }
        }
    }
    else
        CopyFileToClipboard(argv[1]);
    return 0;
}
