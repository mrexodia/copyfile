Overview:
CopyFile is a small program that registers a 'CopyFile' entry
in the context menu of Windows Explorer. It was created because
of this question: http://bit.ly/1cRiFlQ

Usage:
1) Start CopyFile.exe
2) Click 'Yes' when asked if you want to register
3) Right Click -> CopyFile to copy the (ASCII) file